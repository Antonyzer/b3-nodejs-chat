const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);

// Display home page
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', (socket) => {
    
    // Receive new user notification
    socket.on('new_user', (data) => {
        this.username = data;
        socket.broadcast.emit('new_user', data);
    });

    // Receive new message notification
    socket.on('new_message', (data) => {
        data.username = this.username;
        socket.broadcast.emit('new_message', data);
    });

    // Receive user logout notification
    socket.on('logout', (data) => {
        socket.broadcast.emit('logout', this.username);
    });
});

// Start server
http.listen(3000, () => {
    console.log('Server ready on port 3000');
});
